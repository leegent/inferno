from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.validators import validate_email
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from infernoapp.models import MemberProfile


class SignupView(View):
    def get(self, request):
        next = request.GET.get('next', None)
        context = {
            'next': next
        }
        return render(request, 'accountsvcs/signup.html', context)

    def post(self, request):
        email = request.POST['email_field']
        email = email.lower().strip()
        name = request.POST['name_field']
        password1 = request.POST['password1_field']
        password2 = request.POST['password2_field']
        next = request.POST['next']

        try:
            validate_email(email)
        except:
            messages.warning(request, 'Your e-mail address was not in the correct format. Sorry!')
            return render(request, 'accountsvcs/signup.html')

        if password1 != password2:
            messages.warning(request, 'Your passwords did not match!')
            return render(request, 'accountsvcs/signup.html')

        User = get_user_model()
        if User.objects.filter(email=email).exists():
            messages.warning(request, 'This e-mail address is already in use!')
            return render(request, 'accountsvcs/signup.html')

        if User.objects.filter(name=name).exists():
            messages.warning(request, 'This name address is already in use!')
            return render(request, 'accountsvcs/signup.html')

        new_user = User.objects.create_user(email, password=password1)
        new_user.name = name
        new_user.save()

        new_member_profile = MemberProfile.objects.create(user=new_user)

        new_user = authenticate(username=email, password=password1)
        login(request, new_user)

        return HttpResponseRedirect(reverse('home'))


class LoginView(View):
    def get(self, request):
        return render(request, 'accountsvcs/signin.html')

    def post(self, request):
        username = request.POST['username_field']
        password = request.POST['password_field']

        username = username.lower().strip()

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to a success page.
                return HttpResponseRedirect(reverse('home'))
            else:
                return HttpResponse('Account disabled.', content_type='text/plain')
        else:
            # Return an 'invalid login' error message.
            messages.warning(request, 'Your username and password did not match. Sorry!')
            return render(request, 'accountsvcs/signin.html')


@login_required
def logoutview(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))
