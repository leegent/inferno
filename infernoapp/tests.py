from django.contrib.auth import get_user_model
from django.db.models import Count, Sum
from django.test import TestCase
from django.utils import timezone
from django.utils.datetime_safe import date
from datetime import time


from infernoapp.models import Tier, SessionTemplate, MemberProfile, TierEligibility, Session, TierMembership, \
    AttendanceRecord
from infernoapp.views import get_sessions_for_member, get_sessions_with_attendance_for_member


class LogicTest(TestCase):
    def setUp(self):
        # Make three tiers (beginner/intermediate/advanced)
        # Make three templates (beg/int/adv)
        # Make 3 months worth of events
        # Have one member
        # This member was beginner for first month, int for second month and advanced for third
        # What sessions can they attend?
        self.training_user = get_user_model().objects.create_user('training@defaultcityderby.net', password='dummy')
        self.training_member = MemberProfile.objects.create(user=self.training_user)

        self.beginner_tier = Tier.objects.create(name='Beginner')
        self.intermediate_tier = Tier.objects.create(name='Intermediate')
        self.advanced_tier = Tier.objects.create(name='Advanced')
        self.officials_tier = Tier.objects.create(name='Officials')

        self.beginner_session_template = SessionTemplate.objects.create(name='Fresh Meat',
                                                    default_leader=self.training_member,
                                                    default_place='Sports Hall')
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.beginner_tier,
                                       required=True)
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.intermediate_tier,
                                       required=False)
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.advanced_tier,
                                       required=False)

        self.intermediate_session_template = SessionTemplate.objects.create(name='Rising Stars',
                                                    default_leader=self.training_member,
                                                    default_place='Sports Hall')
        TierEligibility.objects.create(session_template=self.intermediate_session_template,
                                       tier=self.intermediate_tier,
                                       required=True)
        TierEligibility.objects.create(session_template=self.intermediate_session_template,
                                       tier=self.advanced_tier,
                                       required=False)

        self.advanced_session_template = SessionTemplate.objects.create(name='Supernovas',
                                                    default_leader=self.training_member,
                                                    default_place='Sports Hall')

        TierEligibility.objects.create(session_template=self.advanced_session_template,
                                       tier=self.advanced_tier,
                                       required=True)

        self.schedule = {
            self.intermediate_session_template: (
                2,
                time(hour=18, minute=30),
                (
                    date(year=2017, month=1, day=3),
                    date(year=2017, month=1, day=10),
                    date(year=2017, month=1, day=17),
                    date(year=2017, month=1, day=24),
                    date(year=2017, month=1, day=31),

                    date(year=2017, month=2, day=7),
                    date(year=2017, month=2, day=14),
                    date(year=2017, month=2, day=21),
                    date(year=2017, month=2, day=28),

                    date(year=2017, month=3, day=7),
                    date(year=2017, month=3, day=14),
                    date(year=2017, month=3, day=21),
                    date(year=2017, month=3, day=28),
                )
            ),
            self.advanced_session_template: (
                3,
                time(hour=20, minute=00),
                (
                    date(year=2017, month=1, day=4),
                    date(year=2017, month=1, day=11),
                    date(year=2017, month=1, day=18),
                    date(year=2017, month=1, day=25),

                    date(year=2017, month=2, day=1),
                    date(year=2017, month=2, day=8),
                    date(year=2017, month=2, day=15),
                    date(year=2017, month=2, day=22),

                    date(year=2017, month=3, day=1),
                    date(year=2017, month=3, day=8),
                    date(year=2017, month=3, day=15),
                    date(year=2017, month=3, day=22),
                    date(year=2017, month=3, day=29),
                )
            ),
            self.beginner_session_template: (
                2,
                time(hour=19, minute=00),
                (
                    date(year=2017, month=1, day=5),
                    date(year=2017, month=1, day=12),
                    date(year=2017, month=1, day=19),
                    date(year=2017, month=1, day=26),

                    date(year=2017, month=2, day=2),
                    date(year=2017, month=2, day=9),
                    date(year=2017, month=2, day=16),
                    date(year=2017, month=2, day=23),

                    date(year=2017, month=3, day=2),
                    date(year=2017, month=3, day=9),
                    date(year=2017, month=3, day=16),
                    date(year=2017, month=3, day=23),
                    date(year=2017, month=3, day=30),
                )
            )
        }

        for tier, tier_shed in self.schedule.items():
            for sesh in tier_shed[2]:
                t = timezone.make_aware(timezone.datetime.combine(sesh, tier_shed[1]))
                Session.objects.create(template=tier, leader=self.training_member, time=t, duration_hours=tier_shed[0])

        test_user = get_user_model().objects.create_user('skater@defaultcityderby.net', password='dummy')
        self.test_member = MemberProfile.objects.create(user=test_user)
        TierMembership.objects.create(member=self.test_member, tier=self.beginner_tier, joined=date(year=2017, month=1, day=1), left=date(year=2017, month=1, day=31))
        TierMembership.objects.create(member=self.test_member, tier=self.officials_tier, joined=date(year=2017, month=1, day=1))
        TierMembership.objects.create(member=self.test_member, tier=self.intermediate_tier, joined=date(year=2017, month=2, day=1), left=date(year=2017, month=3, day=1))
        TierMembership.objects.create(member=self.test_member, tier=self.advanced_tier, joined=date(year=2017, month=3, day=1))

        test_user2 = get_user_model().objects.create_user('skater2@defaultcityderby.net', password='dummy')
        self.test_member2 = MemberProfile.objects.create(user=test_user2)
        TierMembership.objects.create(member=self.test_member2, tier=self.beginner_tier, joined=date(year=2017, month=1, day=1))
        TierMembership.objects.create(member=self.test_member2, tier=self.intermediate_tier, joined=date(year=2017, month=2, day=1))

    def test_create_sessions(self):
        # Alright, we should have created three months worth of sessions
        beginner_sessions_count = len(self.schedule.get(self.beginner_session_template)[2])
        int_sessions_count = len(self.schedule.get(self.beginner_session_template)[2])
        adv_sessions_count = len(self.schedule.get(self.beginner_session_template)[2])

        # So: beginners can go to 13 sessions (and all 13 required)
        # intermediates can go to the beginner sessions (13) and also 13 of their own (for which they are required)
        # advanced can go to beginner 13, int 13 and adv. 13 (for which they are required)
        available_for_beginner = beginner_sessions_count
        available_for_int = available_for_beginner + int_sessions_count
        available_for_adv = available_for_int + adv_sessions_count

        sessions_for_which_beginners_eligable = TierEligibility.objects.filter(tier=self.beginner_tier).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')
        sessions_for_which_intermediate_eligable = TierEligibility.objects.filter(tier=self.intermediate_tier).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')
        sessions_for_which_advanced_eligable = TierEligibility.objects.filter(tier=self.advanced_tier).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')

        sessions_for_which_beginners_required = TierEligibility.objects.filter(tier=self.beginner_tier, required=True).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')
        sessions_for_which_intermediate_required = TierEligibility.objects.filter(tier=self.intermediate_tier, required=True).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')
        sessions_for_which_advanced_required = TierEligibility.objects.filter(tier=self.advanced_tier, required=True).annotate(num_sessions=Count('session_template__session')).aggregate(Sum('num_sessions')).get('num_sessions__sum')


        self.assertEqual(sessions_for_which_beginners_eligable, available_for_beginner)
        self.assertEqual(sessions_for_which_intermediate_eligable, available_for_int)
        self.assertEqual(sessions_for_which_advanced_eligable, available_for_adv)

        self.assertEqual(sessions_for_which_beginners_required, beginner_sessions_count)
        self.assertEqual(sessions_for_which_intermediate_required, int_sessions_count)
        self.assertEqual(sessions_for_which_advanced_required, adv_sessions_count)

    def test_sessions_for_member(self):
        # Arrange

        # Act
        session_list = get_sessions_for_member(self.test_member)

        # Assert
        # In month one I could go to five sessions (beginner) and they were required
        # In month two I could go to eight sessions (beginner + int) of which 4 int are required
        # In month three I could go to 14 sessions (beginner, int, adv) of which 5 adv are required

        # Total freshmeat: 13, of which 4 required
        # Total intermediate: 8, of which 4 required
        # Total advanced: 5, of which 5 required
        freshmeat_all = [s for s in session_list if s.template == self.beginner_session_template]
        freshmeat_required = [s for s in freshmeat_all if s.required]
        self.assertEqual(len(freshmeat_all), 13)
        self.assertEqual(len(freshmeat_required), 4)

        intermediate_all = [s for s in session_list if s.template == self.intermediate_session_template]
        intermediate_required = [ad for ad in intermediate_all if ad.required]
        self.assertEqual(len(intermediate_all), 8)
        self.assertEqual(len(intermediate_required), 4)

        advanced_all = [s for s in session_list if s.template == self.advanced_session_template]
        advanced_required = [ad for ad in advanced_all if ad.required]
        self.assertEqual(len(advanced_all), 5)
        self.assertEqual(len(advanced_required), 5)

        self.assertEqual(len(session_list), 13 + 8 + 5)

    def test_sessions_for_member_are_sorted(self):
        # Arrange
        # Act
        session_list = get_sessions_for_member(self.test_member)
        # Assert
        for x in range(len(session_list)-2):
            self.assertGreaterEqual(session_list[x].time, session_list[x+1].time)

    def test_sessions_for_member_with_attendance_data_length(self):
        session_list = get_sessions_for_member(self.test_member)
        session_list_with_attendance = get_sessions_with_attendance_for_member(self.test_member)
        self.assertEqual(session_list.count(), session_list_with_attendance.count())

    def test_sessions_for_multi_members_with_attendance(self):
        session_list1 = get_sessions_for_member(self.test_member)
        session_list2 = get_sessions_for_member(self.test_member2)
        s1 = set(session_list1)
        s2 = set(session_list2)
        session_list = list(s1.union(s2))

        # Arrange
        #ss = get_sessions_for_member(self.test_member)

        # We'll only attend the first few
        attended_ok = (0, 8)
        attended_late = (1, 7)
        attended_quitter = (2, 6)
        attended_late_and_quitter = (3, 5)
        did_not_attend = (4, 9)

        for i in attended_ok:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=False)
            AttendanceRecord.objects.create(member=self.test_member2, session=s, arrived_late=True, left_early=True)
        for i in attended_late:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=False)
        for i in attended_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=True)
        for i in attended_late_and_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=True)

        # Act
        session_list_with_attendance = get_sessions_with_attendance_for_member(self.test_member)

        # Assert
        freshmeat_all = [s for s in session_list_with_attendance if s.template == self.beginner_session_template]
        freshmeat_required = [s for s in freshmeat_all if s.required]
        self.assertEqual(len(freshmeat_all), 13)
        self.assertEqual(len(freshmeat_required), 4)

        intermediate_all = [s for s in session_list_with_attendance if s.template == self.intermediate_session_template]
        intermediate_required = [ad for ad in intermediate_all if ad.required]
        self.assertEqual(len(intermediate_all), 8)
        self.assertEqual(len(intermediate_required), 4)

        advanced_all = [s for s in session_list_with_attendance if s.template == self.advanced_session_template]
        advanced_required = [ad for ad in advanced_all if ad.required]
        self.assertEqual(len(advanced_all), 5)
        self.assertEqual(len(advanced_required), 5)

        self.assertEqual(len(session_list_with_attendance), 13 + 8 + 5)

    def test_sessions_for_member_with_attendance_data(self):
        # Arrange
        session_list = get_sessions_for_member(self.test_member)

        # We'll only attend the first few
        attended_ok = (0, 8)
        attended_late = (1, 7)
        attended_quitter = (2, 6)
        attended_late_and_quitter = (3, 5)
        did_not_attend = (4, 9)

        for i in attended_ok:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=False)
        for i in attended_late:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=False)
        for i in attended_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=True)
        for i in attended_late_and_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=True)

        # Act
        session_list_with_attendance = get_sessions_with_attendance_for_member(self.test_member)

        # Assert
        for i in attended_ok:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
        for i in attended_late:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.arrived_late)
        for i in attended_quitter:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.left_early)
        for i in attended_late_and_quitter:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.arrived_late)
            self.assertTrue(s1.attendance.left_early)
        for i in did_not_attend:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNone(s1.attendance)

    def test_sessions_for_member_with_attendance_data_multi_tier_event(self):
        # Arrange
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.advanced_tier,
                                       required=False)
        session_list = get_sessions_for_member(self.test_member)

        # We'll only attend the first few
        attended_ok = (0, 8)
        attended_late = (1, 7)
        attended_quitter = (2, 6)
        attended_late_and_quitter = (3, 5)
        did_not_attend = (4, 9)

        for i in attended_ok:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=False)
        for i in attended_late:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=False)
        for i in attended_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=False, left_early=True)
        for i in attended_late_and_quitter:
            s = session_list[i]
            AttendanceRecord.objects.create(member=self.test_member, session=s, arrived_late=True, left_early=True)

        # Act
        session_list_with_attendance = get_sessions_with_attendance_for_member(self.test_member)

        # Assert
        for i in attended_ok:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
        for i in attended_late:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.arrived_late)
        for i in attended_quitter:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.left_early)
        for i in attended_late_and_quitter:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNotNone(s1.attendance)
            self.assertTrue(s1.attendance.arrived_late)
            self.assertTrue(s1.attendance.left_early)
        for i in did_not_attend:
            s0 = session_list[i]
            s1 = session_list_with_attendance[i]
            self.assertEqual(s0.pk, s1.pk)
            self.assertIsNone(s1.attendance)

    def test_sessions_for_member_multi_tier(self):
        # Arrange
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.advanced_tier,
                                       required=False)

        # Act
        session_list = get_sessions_for_member(self.test_member)

        # Assert
        # In month one I could go to five sessions (beginner) and they were required
        # In month two I could go to eight sessions (beginner + int) of which 4 int are required
        # In month three I could go to 14 sessions (beginner, int, adv) of which 5 adv are required

        # Total freshmeat: 13, of which 4 required
        # Total intermediate: 8, of which 4 required
        # Total advanced: 5, of which 5 required
        freshmeat_all = [s for s in session_list if s.template == self.beginner_session_template]
        freshmeat_required = [s for s in freshmeat_all if s.required]
        self.assertEqual(len(freshmeat_all), 13)
        self.assertEqual(len(freshmeat_required), 4)

        intermediate_all = [s for s in session_list if s.template == self.intermediate_session_template]
        intermediate_required = [ad for ad in intermediate_all if ad.required]
        self.assertEqual(len(intermediate_all), 8)
        self.assertEqual(len(intermediate_required), 4)

        advanced_all = [s for s in session_list if s.template == self.advanced_session_template]
        advanced_required = [ad for ad in advanced_all if ad.required]
        self.assertEqual(len(advanced_all), 5)
        self.assertEqual(len(advanced_required), 5)

        self.assertEqual(len(session_list), 13 + 8 + 5)

    def test_sessions_for_member_multi_tier_simultaneous(self):
        # Arrange
        TierEligibility.objects.create(session_template=self.beginner_session_template,
                                       tier=self.officials_tier,
                                       required=False)

        # Act
        session_list = get_sessions_for_member(self.test_member)

        # Assert
        # In month one I could go to five sessions (beginner) and they were required
        # In month two I could go to eight sessions (beginner + int) of which 4 int are required
        # In month three I could go to 14 sessions (beginner, int, adv) of which 5 adv are required

        # Total freshmeat: 13, of which 4 required
        # Total intermediate: 8, of which 4 required
        # Total advanced: 5, of which 5 required
        freshmeat_all = [s for s in session_list if s.template == self.beginner_session_template]
        freshmeat_required = [s for s in freshmeat_all if s.required]
        self.assertEqual(len(freshmeat_all), 13)
        self.assertEqual(len(freshmeat_required), 4)

        intermediate_all = [s for s in session_list if s.template == self.intermediate_session_template]
        intermediate_required = [ad for ad in intermediate_all if ad.required]
        self.assertEqual(len(intermediate_all), 8)
        self.assertEqual(len(intermediate_required), 4)

        advanced_all = [s for s in session_list if s.template == self.advanced_session_template]
        advanced_required = [ad for ad in advanced_all if ad.required]
        self.assertEqual(len(advanced_all), 5)
        self.assertEqual(len(advanced_required), 5)

        self.assertEqual(len(session_list), 13 + 8 + 5)