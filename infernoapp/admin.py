from django.contrib import admin
from django.conf import settings

from . import models


class TMInline(admin.TabularInline):
    model = models.TierMembership
    extra = 0


class TEInline(admin.TabularInline):
    model = models.TierEligibility
    extra = 0


class ARInline(admin.TabularInline):
    model = models.AttendanceRecord
    extra = 0


@admin.register(models.Tier)
class TierAdmin(admin.ModelAdmin):
    inlines = [
        TMInline
    ]


@admin.register(models.TierEligibility)
class TierEligibilityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.TierMembership)
class TierMembershipAdmin(admin.ModelAdmin):
    list_filter = ['member', 'tier']


@admin.register(models.MemberProfile)
class MemberProfileAdmin(admin.ModelAdmin):
    inlines = [
        TMInline
    ]


@admin.register(models.SessionTemplate)
class SessionTemplateAdmin(admin.ModelAdmin):
    list_filter = ['default_leader', 'default_place']
    list_display = ['name', 'default_leader', 'default_place', 'calendar_colour']
    inlines = [
        TEInline
    ]


@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_filter = ['template', 'leader', 'time', 'place']
    ordering = ['-time']
    inlines = [
        ARInline
    ]


@admin.register(models.AttendanceRecord)
class AttendanceRecordAdmin(admin.ModelAdmin):
    list_filter = ['member', 'session']


@admin.register(models.SessionRole)
class SessionRoleAdmin(admin.ModelAdmin):
    pass


@admin.register(models.SessionCommitment)
class SessionCommitmentAdmin(admin.ModelAdmin):
    ordering = ['member']
    list_filter = ['member', 'session', 'role']

admin.site.site_header = f'Inferno Administration :: {settings.LEAGUE_NAME}'
admin.site.site_title = f'Inferno Administration :: {settings.LEAGUE_NAME}'
# admin.site.index_title = 'Add or remove events, roles, visiting attendees or tweak the user account list!'
