# coding=utf-8
from datetime import datetime

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import mixins, generics
from rest_framework import viewsets

from infernoapp.models import Session, Tier, MemberProfile, SessionTemplate, AttendanceRecord, TierEligibility, \
    TierMembership
from infernoapp.serializers import SessionCalendarFeedSerializer, UserSerializer, TierSerializer, \
    MemberProfileSerializer, SessionTemplateSerializer, SessionSerializer, AttendanceRecordSerializer, \
    SessionListSerializer, TierEligibilitySerializer, TierMembershipSerializer, SessionPostSerializer
from infernoapp.views import get_sessions_for_member


class CalendarEventSourceViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pagination_class = None
    serializer_class = SessionCalendarFeedSerializer

    def get_queryset(self):
        start_string = self.request.query_params.get('start', None)
        end_string = self.request.query_params.get('end', None)

        if start_string and end_string:
            start_date = datetime.strptime(start_string, '%Y-%m-%d')
            end_date = datetime.strptime(end_string, '%Y-%m-%d')

            qs = Session.objects.filter(time__gte=start_date, time__lte=end_date)
        else:
            qs = Session.objects.all()
        return qs


class CalendarMemberEventSourceViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pagination_class = None
    serializer_class = SessionCalendarFeedSerializer

    def get_queryset(self):
        user = self.request.user
        memberprofile = user.memberprofile

        start_date = self.request.query_params.get('start', None)
        end_date = self.request.query_params.get('end', None)

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            end_date = datetime.strptime(end_date, '%Y-%m-%d')

        return get_sessions_for_member(memberprofile, start_date, end_date)


class UserProfileView(generics.RetrieveUpdateAPIView):
    serializer_class = MemberProfileSerializer

    def get_object(self):
        user = self.request.user
        return user.memberprofile


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all().order_by('name')
    serializer_class = UserSerializer


class TierViewSet(viewsets.ModelViewSet):
    queryset = Tier.objects.all()
    serializer_class = TierSerializer


class MemberProfileViewSet(viewsets.ModelViewSet):
    queryset = MemberProfile.objects.all().select_related('user').order_by('user__name')
    serializer_class = MemberProfileSerializer


class SessionTemplateViewSet(viewsets.ModelViewSet):
    queryset = SessionTemplate.objects.all()
    serializer_class = SessionTemplateSerializer


class SessionViewSet(viewsets.ModelViewSet):
    queryset = Session.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return SessionListSerializer
        if self.action == 'create':
            return SessionPostSerializer

        return SessionSerializer


class AttendanceRecordViewSet(viewsets.ModelViewSet):
    queryset = AttendanceRecord.objects.all()
    serializer_class = AttendanceRecordSerializer


class TierEligibilityViewSet(viewsets.ModelViewSet):
    queryset = TierEligibility.objects.all()
    serializer_class = TierEligibilitySerializer


class TierMembershipViewSet(viewsets.ModelViewSet):
    queryset = TierMembership.objects.all()
    serializer_class = TierMembershipSerializer


class MembersForSession(viewsets.generics.ListAPIView):
    serializer_class = MemberProfileSerializer

    def get_queryset(self):
        session_id = self.kwargs['session_id']
        s = get_object_or_404(Session, pk=session_id)
        all_eligible_tiers = s.template.tiereligibility_set.all().values('tier')
        members = MemberProfile.objects.filter(tiermembership__tier__in=all_eligible_tiers,
                                               tiermembership__joined__lte=s.time,
                                               tiermembership__left__isnull=True).distinct().order_by('user__name')
        return members

