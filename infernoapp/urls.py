# coding=utf-8
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as authtoken_views
from django.views.generic import TemplateView

from . import views, views_api

api_v1_router = routers.DefaultRouter()
api_v1_router.register(r'users', views_api.UserViewSet)
api_v1_router.register(r'tiers', views_api.TierViewSet)
api_v1_router.register(r'member-profiles', views_api.MemberProfileViewSet)
api_v1_router.register(r'session-templates', views_api.SessionTemplateViewSet)
api_v1_router.register(r'sessions', views_api.SessionViewSet)
api_v1_router.register(r'attendance-records', views_api.AttendanceRecordViewSet)
api_v1_router.register(r'session-template-tier-eligilities', views_api.TierEligibilityViewSet)
api_v1_router.register(r'tier-memberships', views_api.TierMembershipViewSet)
api_v1_router.register(r'calendar-events', views_api.CalendarEventSourceViewSet, base_name='calendar-events')
api_v1_router.register(r'calendar-events-for-member', views_api.CalendarMemberEventSourceViewSet, base_name='calendar-member-events')

api_v1_urls = [
    url('^members-for-session/(?P<session_id>[0-9]+)/$', views_api.MembersForSession.as_view(), name='members_for_session'),
    url('^profile/$', views_api.UserProfileView.as_view()),
    url('^api-token-auth/', authtoken_views.obtain_auth_token),
    url(r'^', include(api_v1_router.urls))
]

api_urls = [
    url(r'^v1/', include(api_v1_urls, namespace='v1'))
]

schema_view = get_schema_view(title='Inferno API')

urlpatterns = [
    url(r'^schema/$', schema_view),
    url(r'^api/', include(api_urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^$', views.home, name='home'),
    url(r'^sessions/$', views.SessionsView.as_view(), name='sessions'),
    url(r'^members/$', views.members, name='members'),
    url(r'^profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^members/(?P<member_id>[0-9]+)/$', views.MemberView.as_view(), name='member'),
    url(r'^register/(?P<session_id>[0-9]+)/$', views.SessionView.as_view(), name='session'),
    url(r'^calendar.ics/$', views.ical, name='ical'),
    url(r'^submit-code/$', views.SubmitCodeView.as_view(), name='submit_code'),
    url(r'create-session/$', views.CreateSessionView.as_view(), name='create_session'),
    url(r'my-sessions/$', views.MySessions.as_view(), name='my_sessions'),
    url(r'my-sessions-calendar/$', TemplateView.as_view(template_name="infernoapp/my-calendar.html"), name='my_sessions_calendar'),
    url(r'^change_attending/$', views.ChangeAttendingView.as_view(), name='change_attending'),
    url(r'^commitments/$', views.CommitmentsView.as_view(), name='commitments'),
    url(r'^commitments/(?P<session_id>[0-9]+)/$', views.CommitmentView.as_view(), name='commitments'),
]
