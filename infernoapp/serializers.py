from datetime import timedelta

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Tier, MemberProfile, SessionTemplate, Session, AttendanceRecord, TierEligibility, TierMembership
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('url', 'email', 'name')


class TierSerializer(serializers. HyperlinkedModelSerializer):
    class Meta:
        model = Tier
        fields = ('url', 'name')


class TierEligibilitySerializer(serializers.HyperlinkedModelSerializer):
    tier = TierSerializer()
    class Meta:
        model = TierEligibility
        fields = ('url', 'id', 'session_template', 'tier', 'required')


class AttendanceRecordSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AttendanceRecord
        fields = ('url', 'member', 'session', 'arrived_late', 'left_early')


class TierMembershipSerializer(serializers.HyperlinkedModelSerializer):
    tier = TierSerializer()
    class Meta:
        model = TierMembership
        fields = ('url', 'id', 'member', 'tier', 'joined', 'left')


class MemberProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    tiermembership_set = TierMembershipSerializer(many=True)

    class Meta:
        model = MemberProfile
        fields = ('url', 'id', 'user', 'tiermembership_set')


class SessionTemplateSerializer(serializers.HyperlinkedModelSerializer):
    tiereligibility_set = TierEligibilitySerializer(many=True)
    default_leader = MemberProfileSerializer()

    class Meta:
        model = SessionTemplate
        fields = ('url', 'name', 'default_leader', 'default_place', 'default_duration_hours', 'tiereligibility_set')


class SessionAttendanceRecordSerializer(serializers.HyperlinkedModelSerializer):
    member = MemberProfileSerializer(read_only=True)

    class Meta:
        model = AttendanceRecord
        fields = ('url', 'member', 'session', 'arrived_late', 'left_early')


class SessionSerializer(serializers.HyperlinkedModelSerializer):
    template = SessionTemplateSerializer(read_only=True)
    attendancerecord_set = SessionAttendanceRecordSerializer(many=True)
    leader = MemberProfileSerializer()
    code_validity_hours = serializers.SerializerMethodField()
    code_validity_ends = serializers.SerializerMethodField()

    def get_code_validity_hours(self, obj):
        return settings.EVENT_CODE_SUBMISSION_LEEWAY_HOURS

    def get_code_validity_ends(self, obj):
        return obj.time + timedelta(hours=self.get_code_validity_hours(obj))

    class Meta:
        model = Session
        fields = ('url', 'id', 'template', 'leader', 'time', 'duration_hours', 'place', 'code', 'code_validity_hours', 'code_validity_ends', 'attendancerecord_set')


class SessionListSerializer(serializers.HyperlinkedModelSerializer):
    template = SessionTemplateSerializer(read_only=True)

    class Meta:
        model = Session
        fields = ('url', 'id', 'template', 'leader', 'time', 'duration_hours', 'place')


class SessionPostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Session
        fields = ('template', 'leader', 'time', 'place', 'duration_hours')


class SessionCalendarFeedSerializer(serializers.BaseSerializer):
    def to_representation(self, instance: Session):
        payload = {
            'id': instance.pk,
            'url': reverse('session', args=[instance.pk]),
            'title': f'{instance.template.name} at {instance.place}',
            'start': instance.time.isoformat(),
            'end': (instance.time + timedelta(hours=instance.duration_hours)).isoformat(),
            'backgroundColor': instance.template.calendar_colour,
        }
        if hasattr(instance, 'required'):
            if instance.required:
                payload['borderColor'] = 'red'
                payload['className'] = 'event-required'
        return payload

