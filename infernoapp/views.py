# coding=utf-8
from typing import Dict, List
from datetime import timedelta

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.db.models import Case
from django.db.models import Q, F, IntegerField, Sum
from django.db.models import Value
from django.db.models import When
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.conf import settings

from infernoapp.models import Session, MemberProfile, AttendanceRecord, TierMembership, SessionTemplate, SessionRole, \
    SessionCommitment


class SessionViewData(object):
    def __init__(self, session, eligible_count, required_count, eligible_attended_count, committed, met_commitments):
        super(SessionViewData, self).__init__()
        self.session: Session = session
        self.eligible_count: int = eligible_count
        self.required_count: int = required_count
        self.eligible_attended_count: int = eligible_attended_count
        self.committed = committed
        self.met_commitments = met_commitments


class MemberViewData(object):
    def __init__(self, member,
                 eligible_count,
                 required_count,
                 eligible_attended_count,
                 required_attended_count,
                 committed_sessions,
                 met_commitments,
                 late_sessions,
                 quitter_sessions):
        super(MemberViewData, self).__init__()
        self.member = member
        self.eligible_count: int = eligible_count
        self.required_count: int = required_count
        self.eligible_attended_count: int = eligible_attended_count
        self.required_attended_count: int = required_attended_count
        self.committed_sessions = committed_sessions
        self.met_commitments = met_commitments
        self.late_sessions = late_sessions
        self.quitter_sessions = quitter_sessions

@login_required()
def home(request):
    context: Dict[str, object] = {}
    member_view_list: List[MemberViewData] = []

    context['members'] = member_view_list

    return render(request, 'infernoapp/home.html', context)


class AttendanceData(object):
    def __init__(self, session, required, attendance):
        super(AttendanceData, self).__init__()
        self.session = session
        self.required = required
        self.attendance = attendance


def get_sessions_for_member(m: MemberProfile, since_date=None, until_date=None):
    # I was in a tier at this time if I joined that tier before the time and hadn't left before that time
    # (was still in the tier, so left > then or left=null)

    query = Session.objects.all()
    if until_date:
        query = query.filter(time__lte=until_date)
    if since_date:
        query = query.filter(time__gte=since_date)

    # Annotate with 'most relevant tier eligibility' defined as: first one where required=True else first.
    ss = query.filter(
        Q(template__tiereligibility__tier__tiermembership__member=m),
        Q(template__tiereligibility__tier__tiermembership__joined__lte=F('time')),
        Q(template__tiereligibility__tier__tiermembership__left__isnull=True) |
        Q(template__tiereligibility__tier__tiermembership__left__gte=F('time'))) \
        .order_by('-time')\
        .distinct()\
        .annotate(
            required=
                Sum(
                    Case(
                        When(
                            template__tiereligibility__required=True,
                            then=1
                        ),
                        default=Value(0),
                        output_field=IntegerField()
                    )
                )
        )

    return ss


def get_sessions_with_attendance_for_member(m: MemberProfile, since_date=None, until_date=None):
    ss = get_sessions_for_member(m, since_date, until_date)
    for s in ss:
        try:
            s.attendance = m.attendancerecord_set.get(session=s)
        except AttendanceRecord.DoesNotExist:
            s.attendance = None

        try:
            s.response = m.sessioncommitment_set.get(session=s)
        except SessionCommitment.DoesNotExist:
            s.response = None
    return ss


@method_decorator(login_required, name='dispatch')
class MemberView(View):
    def get(self, request, member_id):
        m = get_object_or_404(MemberProfile, pk=member_id)
        context: Dict[str, object] = {'member': m, 'session_list': get_sessions_with_attendance_for_member(m, until_date=timezone.now())}

        return render(request, 'infernoapp/member.html', context)


@method_decorator(login_required, name='dispatch')
class SessionView(View):
    def get(self, request, session_id):
        s = get_object_or_404(Session, pk=session_id)
        m = request.user.memberprofile

        context = {
            'session_id': session_id
        }

        if not get_sessions_for_member(m).filter(pk=s.pk).exists():
            template = 'infernoapp/session-ineligible.html'
        else:
            template = 'infernoapp/session.html'

        if s.leader == m:
            template = 'infernoapp/session-owner.html'

        return render(request, template, context)


@method_decorator(login_required, name='dispatch')
class SessionsView(View):
    def get(self, request):
        context: Dict[str, object] = {}
        session_view_dict: List[SessionViewData] = []

        for s in Session.objects.order_by('-time').select_related('template'):
            all_eligible_tiers = s.template.tiereligibility_set.all().values('tier')
            required_tiers = s.template.tiereligibility_set.filter(required=True).values('tier')

            required_count = TierMembership.objects.filter(tier__in=required_tiers,
                                                           joined__lte=s.time,
                                                           left__isnull=True).values('member').distinct().count()
            eligible_count = TierMembership.objects.filter(tier__in=all_eligible_tiers,
                                                           joined__lte=s.time,
                                                           left__isnull=True).values('member').distinct().count()

            attended = s.attendancerecord_set
            attended_count = attended.count()

            committed = s.sessioncommitment_set.all()
            met_commitments = committed.filter(session__in=attended.values('session'))

            session_view_dict.append(SessionViewData(s, eligible_count, required_count, attended_count, committed, met_commitments))

        context['sessions'] = session_view_dict

        return render(request, 'infernoapp/sessionstats.html', context)


@login_required()
def members(request):
    context: Dict[str, object] = {}
    member_view_list: List[MemberViewData] = []

    days: int = None
    if 'days' in request.GET:
        try:
            days = int(request.GET.get('days'))
        except ValueError:
            pass

    now = timezone.now()
    then = None
    if days:
        then = now - timedelta(days=days)

    for m in MemberProfile.objects.select_related('user').order_by('user__name'):
        eligible_sessions = get_sessions_for_member(m, since_date=then, until_date=now)
        eligible_count = eligible_sessions.count()

        required_sessions = eligible_sessions.filter(required=True)
        required_count = required_sessions.count()

        attended_sessions = m.attendancerecord_set.filter(session__in=eligible_sessions)
        attended_count = attended_sessions.count()

        late_sessions = attended_sessions.filter(arrived_late=True)
        quitter_sessions = attended_sessions.filter(left_early=True)

        committed_sessions = m.sessioncommitment_set.filter(session__in=eligible_sessions)
        met_commitments = committed_sessions.filter(session__in=attended_sessions.values('session'))

        required_attended_count = m.attendancerecord_set.filter(session__in=required_sessions).count()

        member_view_list.append(
            MemberViewData(m, eligible_count, required_count, attended_count, required_attended_count, committed_sessions, met_commitments, late_sessions, quitter_sessions)
        )

    context['members'] = member_view_list
    context['days'] = days

    return render(request, 'infernoapp/memberstats.html', context)


def format_ical_date_string(dt):
    return dt.strftime('%Y%m%dT%H%M%SZ')


def ical(request):
    event_duration = timezone.timedelta(hours=settings.EVENT_DURATION_HOURS)
    resp = HttpResponse(content_type='text/calendar')

    resp.write('BEGIN:VCALENDAR\n')
    resp.write('VERSION:2.0\n')
    resp.write('PRODID:-//RDEC//NONSGML v1.0//EN\n')
    now = timezone.now()

    then = now - timedelta(days=settings.CALENDAR_GOBACK_DAYS)

    session_list = Session.objects.filter(time__gt=then).order_by('time')
    for s in session_list:
        end = s.time + event_duration

        resp.write('BEGIN:VEVENT\n')
        resp.write('UID:{}@{}\n'.format(s.pk, request.get_host()))
        resp.write('DTSTAMP:{}\n'.format(format_ical_date_string(s.last_modified)))
        resp.write('DTSTART:{}\n'.format(format_ical_date_string(s.time)))
        resp.write('DTEND:{}\n'.format(format_ical_date_string(end)))
        resp.write('SUMMARY:{} ({})\n'.format(s.template.name, s.place))

        resp.write('END:VEVENT\n')

    resp.write('END:VCALENDAR\n')
    return resp


@method_decorator(login_required, name='dispatch')
class ProfileView(View):
    def get(self, request):
        return render(request, 'infernoapp/profile.html')

    def post(self, request):
        User = get_user_model()
        user = request.user
        changed = False

        if 'change_name' in request.POST:
            new_name = request.POST['change_name']
            if new_name:
                existing = User.objects.filter(name=new_name).exists()
                if existing:
                    messages.warning(request, 'The name you supplied has already been used!')
                else:
                    user.name = new_name
                    changed = True
        if 'change_mail_1' in request.POST and 'change_mail_2' in request.POST:
            m1 = request.POST['change_mail_1']
            m2 = request.POST['change_mail_2']
            if m1 and m2:
                if m1 != m2:
                    messages.warning(request, 'The email addresses provided did not match!')
                else:
                    existing = User.objects.filter(email=m1).exists()
                    if existing:
                        messages.warning(request, 'The email address you supplied has already been used!')
                    else:
                        user.email = m1
                        user.username = m1
                        changed = True

        if 'change_password_1' in request.POST and 'change_password_2' in request.POST:
            p1 = request.POST['change_password_1']
            p2 = request.POST['change_password_2']
            if p1 and p2:
                if p1 != p2:
                    messages.warning(request, 'The passwords provided did not match!')
                else:
                    user.set_password(p1)
                    changed = True

        if changed:
            user.save()
            messages.info(request, 'Profile Saved!')

        return render(request, 'infernoapp/profile.html')


@method_decorator(login_required, name='dispatch')
class SubmitCodeView(View):
    def post(self, request):
        user = request.user
        m = user.memberprofile

        if 'session_id' not in request.POST:
            return HttpResponseBadRequest('Missing data: session_id', content_type='text/plain')
        if 'code' not in request.POST:
            return HttpResponseBadRequest('Missing data: code', content_type='text/plain')
        session_id = int(request.POST['session_id'])
        submitted_code = int(request.POST['code'])

        s = get_object_or_404(Session, pk=session_id)

        if s.code != submitted_code:
            return HttpResponseForbidden('The code is incorrect.', content_type='text/plain')

        now = timezone.now()
        leeway = timedelta(hours=settings.EVENT_CODE_SUBMISSION_LEEWAY_HOURS)
        if now > (s.time + leeway):
            return HttpResponseForbidden('The code is no longer valid.', content_type='text/plain')

        if not get_sessions_for_member(m).filter(pk=s.pk).exists():
            return HttpResponseForbidden('You are not eligible for this session.', content_type='text/plain')

        att, created = AttendanceRecord.objects.get_or_create(member=m, session=s)

        return HttpResponse()


@method_decorator(login_required, name='dispatch')
class CreateSessionView(View):
    def get(self, request):
        u = request.user
        m = u.memberprofile

        # Show form with all templates
        # On template select, fill form with template defaults
        t = SessionTemplate.objects.all()
        m = MemberProfile.objects.all()
        return render(request, 'infernoapp/create-session.html', {'templates': t, 'members': m})

    def post(self, request):
        pass


@method_decorator(login_required, name='dispatch')
class MySessions(View):
    def get(self, request):
        u = request.user
        m = u.memberprofile
        now = timezone.now() - timedelta(days=1)

        sessions = get_sessions_with_attendance_for_member(m, since_date=now)
        context = {
            'sessions': sessions,
            'roles': SessionRole.objects.all(),
        }
        return render(request, 'infernoapp/my-sessions.html', context)


@method_decorator(login_required, name='dispatch')
class ChangeAttendingView(View):
    def post(self, request):
        event_id = request.POST['event_id']
        new_status = request.POST['new_status']
        user = request.user
        member = user.memberprofile

        session = Session.objects.get(pk=event_id)
        if new_status == 'null':
            SessionCommitment.objects.filter(member=member, session=session).delete()
            return HttpResponse()

        status = SessionRole.objects.get(pk=new_status)

        SessionCommitment.objects.filter(member=member, session=session).delete()  # TODO: fix this bug properly!

        thing = SessionCommitment(member=member, session=session, role=status)
        thing.save()
        return HttpResponse()


class _SessionCommitmentSummary(object):
    def __init__(self, session):
        self.session = session
        self.role_map = dict()


class _SessionCommitmentViewData(object):
    def __init__(self, session, eligible_count, required_count, committed, scs):
        super(_SessionCommitmentViewData, self).__init__()
        self.session: Session = session
        self.eligible_count: int = eligible_count
        self.required_count: int = required_count
        self.committed = committed
        self.scs = scs


@method_decorator(login_required, name='dispatch')
class CommitmentsView(View):
    def get(self, request):
        context = {}
        session_view_dict: List[_SessionCommitmentViewData] = []

        now = timezone.now()
        since = now - timedelta(days=1)
        roles = SessionRole.objects.all()

        for s in Session.objects.filter(time__gte=since).order_by('-time').select_related('template'):
            all_eligible_tiers = s.template.tiereligibility_set.all().values('tier')
            required_tiers = s.template.tiereligibility_set.filter(required=True).values('tier')

            required_count = TierMembership.objects.filter(tier__in=required_tiers,
                                                           joined__lte=s.time,
                                                           left__isnull=True).values('member').distinct().count()
            eligible_count = TierMembership.objects.filter(tier__in=all_eligible_tiers,
                                                           joined__lte=s.time,
                                                           left__isnull=True).values('member').distinct().count()

            committed = s.sessioncommitment_set.all()

            scs = _SessionCommitmentSummary(s)

            for role in roles:
                scs.role_map[role] = list()

            for commitment in committed.all():
                scs.role_map[commitment.role].append(commitment)

            session_view_dict.append(_SessionCommitmentViewData(s, eligible_count, required_count, committed, scs))

        context['sessions'] = session_view_dict
        context['roles'] = SessionRole.objects.all()

        return render(request, 'infernoapp/session-commitments.html', context)


@method_decorator(login_required, name='dispatch')
class CommitmentView(View):
    def get(self, request, session_id):
        session = get_object_or_404(Session, pk=session_id)
        roles = SessionRole.objects.all()

        session_map = dict()

        for role in roles:
            session_map[role] = list()
        for member_attending in session.sessioncommitment_set.all():
            session_map[member_attending.role].append(member_attending)

        context = {
            'session': session,
            'roles': roles,
            'sessionmap': session_map
        }
        return render(request, 'infernoapp/session-commitments-detail.html', context)
