from django.apps import AppConfig


class InfernoappConfig(AppConfig):
    name = 'infernoapp'
