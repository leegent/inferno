import random

from django.db import models
from django.conf import settings
from django.db.models import CASCADE
from django.db.models import SET_NULL
from django.utils import timezone
from datetime import timedelta


class Tier(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name}'


class MemberProfile(models.Model):
    class Meta:
        ordering = ["user__name"]
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=CASCADE)

    # We would like to store a list of tiers, recoding the dates we joined and the dates we left (if any)

    def __str__(self):
        return f'{self.user.name}'


class TierMembership(models.Model):
    class Meta:
        ordering = ["tier"]
    member = models.ForeignKey(MemberProfile, on_delete=CASCADE)
    tier = models.ForeignKey(Tier, on_delete=CASCADE)
    joined = models.DateField(default=timezone.now)
    left = models.DateField(null=True, blank=True)

    def __str__(self):
        return f'{self.member} {self.tier} membership'

COLOURS = [
            "aliceblue",
            "antiquewhite",
            "aqua",
            "aquamarine",
            "azure",
            "beige",
            "bisque",
            "black",
            "blanchedalmond",
            "blue",
            "blueviolet",
            "brown",
            "burlywood",
            "cadetblue",
            "chartreuse",
            "chocolate",
            "coral",
            "cornflowerblue",
            "cornsilk",
            "crimson",
            "cyan",
            "darkblue",
            "darkcyan",
            "darkgoldenrod",
            "darkgray",
            "darkgreen",
            "darkkhaki",
            "darkmagenta",
            "darkolivegreen",
            "darkorange",
            "darkorchid",
            "darkred",
            "darksalmon",
            "darkseagreen",
            "darkslateblue",
            "darkslategray",
            "darkturquoise",
            "darkviolet",
            "deeppink",
            "deepskyblue",
            "dimgray",
            "dodgerblue",
            "firebrick",
            "floralwhite",
            "forestgreen",
            "fuchsia",
            "gainsboro",
            "ghostwhite",
            "gold",
            "goldenrod",
            "gray",
            "green",
            "greenyellow",
            "honeydew",
            "hotpink",
            "indianred",
            "indigo",
            "ivory",
            "khaki",
            "lavender",
            "lavenderblush",
            "lawngreen",
            "lemonchiffon",
            "lightblue",
            "lightcoral",
            "lightcyan",
            "lightgoldenrodyellow",
            "lightgray",
            "lightgreen",
            "lightpink",
            "lightsalmon",
            "lightseagreen",
            "lightskyblue",
            "lightslategray",
            "lightsteelblue",
            "lightyellow",
            "lime",
            "limegreen",
            "linen",
            "magenta",
            "maroon",
            "mediumaquamarine",
            "mediumblue",
            "mediumorchid",
            "mediumpurple",
            "mediumseagreen",
            "mediumslateblue",
            "mediumspringgreen",
            "mediumturquoise",
            "mediumvioletred",
            "midnightblue",
            "mintcream",
            "mistyrose",
            "moccasin",
            "navajowhite",
            "navy",
            "oldlace",
            "olive",
            "olivedrab",
            "orange",
            "orangered",
            "orchid",
            "palegoldenrod",
            "palegreen",
            "paleturquoise",
            "palevioletred",
            "papayawhip",
            "peachpuff",
            "peru",
            "pink",
            "plum",
            "powderblue",
            "purple",
            "rebeccapurple",
            "red",
            "rosybrown",
            "royalblue",
            "saddlebrown",
            "salmon",
            "sandybrown",
            "seagreen",
            "seashell",
            "sienna",
            "silver",
            "skyblue",
            "slateblue",
            "slategray",
            "snow",
            "springgreen",
            "steelblue",
            "tan",
            "teal",
            "thistle",
            "tomato",
            "turquoise",
            "violet",
            "wheat",
            "whitesmoke",
            "yellow",
            "yellowgreen"
]


def colour_picker():
    return random.choice(COLOURS)


class SessionTemplate(models.Model):
    name = models.CharField(max_length=200)
    default_leader = models.ForeignKey(MemberProfile, null=True, on_delete=SET_NULL)
    default_place = models.CharField(max_length=150)
    default_duration_hours = models.IntegerField(default=2)
    calendar_colour = models.CharField(max_length=50, default=colour_picker)

    def __str__(self):
        return f'Session Template: {self.name}'


class TierEligibility(models.Model):
    class Meta:
        verbose_name_plural = 'Tier Eligibilities'
    session_template = models.ForeignKey(SessionTemplate)
    tier = models.ForeignKey(Tier)
    required = models.BooleanField()

    def __str__(self):
        return f'{self.tier} eligibility for {self.session_template} ({"not " if not self.required else ""}required)'


def generate_code():
    return random.SystemRandom().randrange(999)


class Session(models.Model):
    class Meta:
        ordering = ["time"]

    template = models.ForeignKey(SessionTemplate)
    leader = models.ForeignKey(MemberProfile, null=True, related_name='leader_for_sessions')
    time = models.DateTimeField()
    place = models.CharField(max_length=150)
    last_modified = models.DateTimeField(auto_now=True)
    code = models.IntegerField(default=generate_code)
    duration_hours = models.IntegerField(default=2)

    attendances = models.ManyToManyField(MemberProfile, through='AttendanceRecord')
    commitments = models.ManyToManyField(MemberProfile, through='SessionCommitment', related_name='committed_sessions')

    @property
    def until(self):
        return self.time + timedelta(hours=self.duration_hours)

    def __str__(self):
        return f'Session {self.template.name} at {self.place} on {self.time} with {self.leader}'


class AttendanceRecord(models.Model):
    class Meta:
        ordering = ["member__user__name"]

    member = models.ForeignKey(MemberProfile, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    arrived_late = models.BooleanField(default=False)
    left_early = models.BooleanField(default=False)

    def __str__(self):
        return f'Attendance for {self.member}, {self.session}'


class SessionRole(models.Model):
    role = models.CharField(max_length=50)

    def __str__(self):
        return self.role


class SessionCommitment(models.Model):
    member = models.ForeignKey(MemberProfile, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    role = models.ForeignKey(SessionRole, on_delete=models.CASCADE)
