# inferno
[![build status](https://gitlab.com/leegent/inferno/badges/master/build.svg)](https://gitlab.com/leegent/inferno/commits/master)
[![coverage report](https://gitlab.com/leegent/inferno/badges/master/coverage.svg)](https://gitlab.com/leegent/inferno/commits/master)


**_A Roller Derby league training session and event tracking application_**

Inferno is a Web application designed for Roller Derby leagues to help them monitor and manage the attendance of their
members to training sessions.

### Configuration
Major configuration is via environment variables.
````DATABASE_URL='sqlite:///inferno.db'
DATABASE_URL='postgres://postgresuser:postgrespass@localhost:5432/rdecdb'
ALLOWED_HOSTS='.roller-derby.rocks inferno.herokuapp.com'
DEBUG=False
LEAGUE_NAME='Hellfire Harlots'
````
